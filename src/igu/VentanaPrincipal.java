package igu;

import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.border.LineBorder;

import logica.Carrera;
import logica.Corredor;
import logica.Dado;

import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class VentanaPrincipal extends JFrame {
	private JPanel contentPane;

	private JButton botonDado;

	private JPanel panelLiebre;
	private JPanel panelTortuga;

	private JButton boton0L;
	private JButton boton1L;
	private JButton boton2L;
	private JButton boton3L;
	private JButton boton4L;
	private JButton boton5L;
	private JButton boton6L;
	private JButton boton7L;
	private JButton boton8L;
	private JButton boton9L;
	private JButton boton10L;

	private JButton boton0T;
	private JButton boton1T;
	private JButton boton2T;
	private JButton boton3T;
	private JButton boton4T;
	private JButton boton5T;
	private JButton boton6T;
	private JButton boton7T;
	private JButton boton8T;
	private JButton boton9T;
	private JButton boton10T;

	private Carrera carrera;

	private JTextField textoPuntuacionLiebre;
	private JTextField textoPuntuacionTortuga;

	private JLabel etiquetaScore;
	private JLabel etiquetaDado;

	private JLabel etiquetaLiebre;
	private JLabel etiquetaTortuga;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaPrincipal frame = new VentanaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public VentanaPrincipal() {
		carrera = new Carrera();
		setBackground(Color.WHITE);
		setTitle("El Juego de la Liebre y la Tortuga");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 410);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLUE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);

		contentPane.setLayout(null);

		contentPane.add(getEtiquetaDado());
		contentPane.add(getEtiquetaScore());

		contentPane.add(getTextoPuntuacionLiebre());
		contentPane.add(getTextoPuntuacionTortuga());

		contentPane.add(getBotonDado());

		contentPane.add(getPanelLiebre());
		contentPane.add(getPanelTortuga());

		contentPane.add(getEtiquetaLiebre());
		contentPane.add(getEtiquetaTortuga());

		representarEstadoJuego();
	}

	private JButton getBotonDado() {
		if (botonDado == null) {
			botonDado = new JButton("");
			botonDado.setBackground(Color.BLACK);
			botonDado.setDisabledIcon(new ImageIcon(VentanaPrincipal.class.getResource("/img/dado.JPG")));
			botonDado.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					lanzarDado();
				}
			});
			botonDado.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/img/dado.JPG")));
			botonDado.setBounds(42, 30, 69, 92);
		}
		return botonDado;
	}

	private void lanzarDado() {
		boolean jugadaPosible = carrera.lanzarDado();
		etiquetaDado.setText(String.valueOf(Dado.getValor()));

		if (jugadaPosible) {
			if (carrera.getCorredorActivo().equals(carrera.getLiebre()))
				habilitarPanel(panelLiebre);
			else
				habilitarPanel(panelTortuga);
			botonDado.setEnabled(false);
		} else {
			JOptionPane.showMessageDialog(this, "La jugada no es posible. Cambio de turno.");
			etiquetaDado.setText("");
		}
	}

	private JPanel getPanelLiebre() {
		if (panelLiebre == null) {
			panelLiebre = new JPanel();
			panelLiebre.setForeground(Color.WHITE);
			panelLiebre.setBorder(new LineBorder(Color.BLUE, 4));
			panelLiebre.setBackground(Color.BLUE);
			panelLiebre.setBounds(21, 151, 835, 80);
			panelLiebre.setLayout(new GridLayout(1, 11, 2, 0));
			panelLiebre.add(getBoton0L());
			panelLiebre.add(getBoton1L());
			panelLiebre.add(getBoton2L());
			panelLiebre.add(getBoton3L());
			panelLiebre.add(getBoton4L());
			panelLiebre.add(getBoton5L());
			panelLiebre.add(getBoton6L());
			panelLiebre.add(getBoton7L());
			panelLiebre.add(getBoton8L());
			panelLiebre.add(getBoton9L());
			panelLiebre.add(getBoton10L());
			deshabilitarPanel(panelLiebre);
		}
		return panelLiebre;
	}

	private JPanel getPanelTortuga() {
		if (panelTortuga == null) {
			panelTortuga = new JPanel();
			panelTortuga.setForeground(Color.WHITE);
			panelTortuga.setBorder(new LineBorder(Color.BLUE, 4));
			panelTortuga.setBackground(Color.BLUE);
			panelTortuga.setBounds(21, 256, 835, 80);
			panelTortuga.setLayout(new GridLayout(1, 11, 2, 0));
			panelTortuga.add(getBoton0T());
			panelTortuga.add(getBoton1T());
			panelTortuga.add(getBoton2T());
			panelTortuga.add(getBoton3T());
			panelTortuga.add(getBoton4T());
			panelTortuga.add(getBoton5T());
			panelTortuga.add(getBoton6T());
			panelTortuga.add(getBoton7T());
			panelTortuga.add(getBoton8T());
			panelTortuga.add(getBoton9T());
			panelTortuga.add(getBoton10T());
			deshabilitarPanel(panelTortuga);
		}
		return panelTortuga;
	}

	private JButton getBoton0L() {
		if (boton0L == null) {
			boton0L = new JButton("");
			boton0L.setBackground(Color.BLACK);
		}
		return boton0L;
	}

	private JButton getBoton1L() {
		if (boton1L == null) {
			boton1L = new JButton("");
			boton1L.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jugar(1);
				}
			});
			boton1L.setBackground(Color.BLACK);
		}
		return boton1L;
	}

	private JButton getBoton2L() {
		if (boton2L == null) {
			boton2L = new JButton("");
			boton2L.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jugar(2);
				}
			});
			boton2L.setBackground(Color.BLACK);
		}
		return boton2L;
	}

	private JButton getBoton3L() {
		if (boton3L == null) {
			boton3L = new JButton("");
			boton3L.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jugar(3);
				}
			});
			boton3L.setBackground(Color.BLACK);
		}
		return boton3L;
	}

	private JButton getBoton4L() {
		if (boton4L == null) {
			boton4L = new JButton("");
			boton4L.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jugar(4);
				}
			});
			boton4L.setBackground(Color.BLACK);
		}
		return boton4L;
	}

	private JButton getBoton5L() {
		if (boton5L == null) {
			boton5L = new JButton("");
			boton5L.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jugar(5);
				}
			});
			boton5L.setBackground(Color.BLACK);
		}
		return boton5L;
	}

	private JButton getBoton6L() {
		if (boton6L == null) {
			boton6L = new JButton("");
			boton6L.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jugar(6);
				}
			});
			boton6L.setBackground(Color.BLACK);
		}
		return boton6L;
	}

	private JButton getBoton7L() {
		if (boton7L == null) {
			boton7L = new JButton("");
			boton7L.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jugar(7);
				}
			});
			boton7L.setBackground(Color.BLACK);
		}
		return boton7L;
	}

	private JButton getBoton8L() {
		if (boton8L == null) {
			boton8L = new JButton("");
			boton8L.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jugar(8);
				}
			});
			boton8L.setBackground(Color.BLACK);
		}
		return boton8L;
	}

	private JButton getBoton9L() {
		if (boton9L == null) {
			boton9L = new JButton("");
			boton9L.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jugar(9);
				}
			});
			boton9L.setBackground(Color.BLACK);
		}
		return boton9L;
	}

	private JButton getBoton10L() {
		if (boton10L == null) {
			boton10L = new JButton("");
			boton10L.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jugar(10);
				}
			});
			boton10L.setBackground(Color.BLACK);
		}
		return boton10L;
	}

	private JButton getBoton0T() {
		if (boton0T == null) {
			boton0T = new JButton("");
			boton0T.setBackground(Color.BLACK);
		}
		return boton0T;
	}

	private JButton getBoton1T() {
		if (boton1T == null) {
			boton1T = new JButton("");
			boton1T.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jugar(1);
				}
			});
			boton1T.setBackground(Color.BLACK);
		}
		return boton1T;
	}

	private JButton getBoton2T() {
		if (boton2T == null) {
			boton2T = new JButton("");
			boton2T.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jugar(2);
				}
			});
			boton2T.setBackground(Color.BLACK);
		}
		return boton2T;
	}

	private JButton getBoton3T() {
		if (boton3T == null) {
			boton3T = new JButton("");
			boton3T.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jugar(3);
				}
			});
			boton3T.setBackground(Color.BLACK);
		}
		return boton3T;
	}

	private JButton getBoton4T() {
		if (boton4T == null) {
			boton4T = new JButton("");
			boton4T.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jugar(4);
				}
			});
			boton4T.setBackground(Color.BLACK);
		}
		return boton4T;
	}

	private JButton getBoton5T() {
		if (boton5T == null) {
			boton5T = new JButton("");
			boton5T.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jugar(5);
				}
			});
			boton5T.setBackground(Color.BLACK);
		}
		return boton5T;
	}

	private JButton getBoton6T() {
		if (boton6T == null) {
			boton6T = new JButton("");
			boton6T.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jugar(6);
				}
			});
			boton6T.setBackground(Color.BLACK);
		}
		return boton6T;
	}

	private JButton getBoton7T() {
		if (boton7T == null) {
			boton7T = new JButton("");
			boton7T.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jugar(7);
				}
			});
			boton7T.setBackground(Color.BLACK);
		}
		return boton7T;
	}

	private JButton getBoton8T() {
		if (boton8T == null) {
			boton8T = new JButton("");
			boton8T.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jugar(8);
				}
			});
			boton8T.setBackground(Color.BLACK);
		}
		return boton8T;
	}

	private JButton getBoton9T() {
		if (boton9T == null) {
			boton9T = new JButton("");
			boton9T.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jugar(9);
				}
			});
			boton9T.setBackground(Color.BLACK);
		}
		return boton9T;
	}

	private JButton getBoton10T() {
		if (boton10T == null) {
			boton10T = new JButton("");
			boton10T.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jugar(10);
				}
			});
			boton10T.setBackground(Color.BLACK);
		}
		return boton10T;
	}

	private void jugar(int posicion) {
		if (carrera.resolverJugada(posicion)) {
			representarEstadoJuego();
			deshabilitarPanel(panelLiebre);
			deshabilitarPanel(panelTortuga);
		}
	}

	private void representarEstadoJuego() {
		// Pintar movimiento del corredor.
		pintarCorredor();

		// Caer en la trampa.
		if (carrera.caisteEnLaTrampa(carrera.getCorredorNoActivo().getPosicion())) {
			// Te salvaste. Sigue la partida.
			if (carrera.teSalvaste(carrera.getCorredorNoActivo().getPosicion())) {
				carrera.getCorredorNoActivo().sobrevivirTrampa();
				pintarPuntos();
				JOptionPane.showMessageDialog(null,
						"La " + carrera.getCorredorNoActivo().getNombre() + " se ha salvado.");
				botonDado.setEnabled(true);
				etiquetaDado.setText("");
			}

			// Te mueres. Termina la partida.
			else {
				carrera.getCorredorNoActivo().caerEnLaTrampa();
				pintarPuntos();
				JOptionPane.showMessageDialog(null,
						"La " + carrera.getCorredorNoActivo().getNombre() + " ha caido en la trampa.");
				deshabilitarPanel(panelLiebre);
				deshabilitarPanel(panelTortuga);
				botonDado.setEnabled(false);
			}
		}

		// Llegaste a la meta.
		else if (carrera.isPartidaFinalizada()) {
			if (carrera.getCorredorActivo().equals(carrera.getLiebre())) {
				pintarPuntos();
				JOptionPane.showMessageDialog(null, "La tortuga gana");
			} else {
				pintarPuntos();
				JOptionPane.showMessageDialog(null, "La liebre gana");
			}
			etiquetaDado.setText("");
		}

		// La partida sigue.
		else {
			// Caiste en el superpoder.
			if (carrera.caisteEnElSuperpoder(carrera.getCorredorNoActivo().getPosicion())) {
				carrera.getCorredorNoActivo().recogerSuperpoder();
				pintarPuntos();
				JOptionPane.showMessageDialog(null,
						"Ahora la " + carrera.getCorredorNoActivo().getNombre() + " tiene superpoderes.");
			}

			// Caiste en el arbol.
			else if (carrera.caisteEnArbol(carrera.getLiebre().getPosicion())
					&& carrera.getCorredorNoActivo() == carrera.getLiebre()) {
				pintarPuntos();
				JOptionPane.showMessageDialog(null,
						"La " + carrera.getCorredorNoActivo().getNombre() + " se ha parado a dormir");
			}

			pintarPuntos();
			botonDado.setEnabled(true);
			etiquetaDado.setText("");
		}
	}

	private void pintarCorredor() {
		pintarCalle(carrera.getLiebre(), panelLiebre.getComponents());
		pintarCalle(carrera.getTortuga(), panelTortuga.getComponents());
	}

	private void pintarCalle(Corredor corredor, Component[] botones) {
		ImageIcon imagen = new ImageIcon(getClass().getResource("/img/" + corredor.getFoto()));

		for (int i = 0; i < botones.length - 1; i++) {
			JButton boton = (JButton) botones[i];
			if (corredor == carrera.getLiebre() && i == carrera.getCasillaArbol()
					&& carrera.getLiebre().getPosicion() == carrera.getCasillaArbol()) {
				boton.setIcon(new ImageIcon(getClass().getResource("/img/liebre_durmiendo.jpg")));
				boton.setDisabledIcon(new ImageIcon(getClass().getResource("/img/liebre_durmiendo.jpg")));
			} else if (i == corredor.getPosicion()) {
				boton.setIcon(imagen);
				boton.setDisabledIcon(imagen);
			} else if (corredor == carrera.getLiebre() && i == carrera.getCasillaArbol()) {
				boton.setIcon(new ImageIcon(getClass().getResource("/img/arbol.jpg")));
				boton.setDisabledIcon(new ImageIcon(getClass().getResource("/img/arbol.jpg")));
			} else {
				boton.setIcon(null);
				boton.setDisabledIcon(null);
			}
		}

		if (corredor.getPosicion() == botones.length - 1) {
			((JButton) botones[botones.length - 1]).setIcon(new ImageIcon(getClass().getResource("/img/victoria.jpg")));
			((JButton) botones[botones.length - 1])
					.setDisabledIcon(new ImageIcon(getClass().getResource("/img/victoria.jpg")));
		} else {
			((JButton) botones[botones.length - 1])
					.setIcon(new ImageIcon(getClass().getResource("/img/zanahoria.jpg")));
			((JButton) botones[botones.length - 1])
					.setDisabledIcon(new ImageIcon(getClass().getResource("/img/zanahoria.jpg")));
		}
	}

	private void pintarPuntos() {
		textoPuntuacionLiebre.setText(String.valueOf(carrera.getLiebre().getPuntuacion()));
		textoPuntuacionTortuga.setText(String.valueOf(carrera.getTortuga().getPuntuacion()));
	}

	private void modificarPanel(JPanel panel, boolean habilitar) {
		for (Component componente : panel.getComponents())
			componente.setEnabled(habilitar);
	}

	private void deshabilitarPanel(JPanel panel) {
		modificarPanel(panel, false);
	}

	private void habilitarPanel(JPanel panel) {
		modificarPanel(panel, true);
	}

	private JTextField getTextoPuntuacionLiebre() {
		if (textoPuntuacionLiebre == null) {
			textoPuntuacionLiebre = new JTextField();
			textoPuntuacionLiebre.setForeground(Color.WHITE);
			textoPuntuacionLiebre.setHorizontalAlignment(SwingConstants.CENTER);
			textoPuntuacionLiebre.setSelectionColor(Color.RED);
			textoPuntuacionLiebre.setDisabledTextColor(Color.WHITE);
			textoPuntuacionLiebre.setBackground(Color.GRAY);
			textoPuntuacionLiebre.setEditable(false);
			textoPuntuacionLiebre.setFont(new Font("Dialog", Font.PLAIN, 30));
			textoPuntuacionLiebre.setBorder(new LineBorder(new Color(64, 64, 64), 8));
			textoPuntuacionLiebre.setBounds(701, 33, 129, 40);
			textoPuntuacionLiebre.setColumns(1);
		}
		return textoPuntuacionLiebre;
	}

	private JLabel getEtiquetaScore() {
		if (etiquetaScore == null) {
			etiquetaScore = new JLabel("Score");
			etiquetaScore.setFont(new Font("Dialog", Font.PLAIN, 70));
			etiquetaScore.setForeground(Color.BLUE);
			etiquetaScore.setHorizontalAlignment(SwingConstants.CENTER);
			etiquetaScore.setBounds(394, 30, 183, 89);
		}
		return etiquetaScore;
	}

	private JLabel getEtiquetaDado() {
		if (etiquetaDado == null) {
			etiquetaDado = new JLabel("");
			etiquetaDado.setFont(new Font("Unispace", Font.PLAIN, 40));
			etiquetaDado.setHorizontalAlignment(SwingConstants.CENTER);
			etiquetaDado.setForeground(Color.WHITE);
			etiquetaDado.setBackground(Color.BLACK);
			etiquetaDado.setBounds(172, 44, 119, 65);
		}
		return etiquetaDado;
	}

	private JTextField getTextoPuntuacionTortuga() {
		if (textoPuntuacionTortuga == null) {
			textoPuntuacionTortuga = new JTextField();
			textoPuntuacionTortuga.setSelectionColor(Color.RED);
			textoPuntuacionTortuga.setHorizontalAlignment(SwingConstants.CENTER);
			textoPuntuacionTortuga.setForeground(Color.WHITE);
			textoPuntuacionTortuga.setFont(new Font("Dialog", Font.PLAIN, 30));
			textoPuntuacionTortuga.setEditable(false);
			textoPuntuacionTortuga.setDisabledTextColor(Color.WHITE);
			textoPuntuacionTortuga.setColumns(1);
			textoPuntuacionTortuga.setBorder(new LineBorder(new Color(64, 64, 64), 8));
			textoPuntuacionTortuga.setBackground(Color.GRAY);
			textoPuntuacionTortuga.setBounds(701, 81, 129, 40);
		}
		return textoPuntuacionTortuga;
	}

	private JLabel getEtiquetaLiebre() {
		if (etiquetaLiebre == null) {
			etiquetaLiebre = new JLabel("");
			etiquetaLiebre.setHorizontalAlignment(SwingConstants.CENTER);
			etiquetaLiebre.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/img/liebre_peq.JPG")));
			etiquetaLiebre.setBounds(646, 37, 35, 33);
		}
		return etiquetaLiebre;
	}

	private JLabel getEtiquetaTortuga() {
		if (etiquetaTortuga == null) {
			etiquetaTortuga = new JLabel("");
			etiquetaTortuga.setHorizontalAlignment(SwingConstants.CENTER);
			etiquetaTortuga.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/img/tortuga_peq.JPG")));
			etiquetaTortuga.setBounds(647, 84, 32, 35);
		}
		return etiquetaTortuga;
	}

}
