package logica;

public class ClasePrueba {

	private float atributoPrueba;

	public ClasePrueba(float atributoPrueba) {
		this.atributoPrueba = atributoPrueba;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(atributoPrueba);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClasePrueba other = (ClasePrueba) obj;
		if (Float.floatToIntBits(atributoPrueba) != Float.floatToIntBits(other.atributoPrueba))
			return false;
		return true;
	}

	public float getAtributoPrueba() {
		return atributoPrueba;
	}

	public void setAtributoPrueba(float atributoPrueba) {
		this.atributoPrueba = atributoPrueba;
	}

	@Override
	public String toString() {
		return "ClasePrueba [atributoPrueba=" + atributoPrueba + ", hashCode()=" + hashCode() + ", getAtributoPrueba()="
				+ getAtributoPrueba()+ "]";
	}

}
