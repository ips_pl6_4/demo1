package logica;

public class Carrera {

	private Corredor liebre;
	private Corredor tortuga;

	private Corredor corredorActivo;

	private int casillaTrampa;
	private int casillaSuperpoder;
	private int casillaArbol;
	private int prueba;

	private boolean trampaParaLiebre;
	private boolean trampaParaTortuga;

	public Carrera() {
		inicializarJuego();
	}

	public void inicializarJuego() {
		liebre = new Corredor(new Calle(), "liebre", "liebre.jpg", 4, false);
		tortuga = new Corredor(new Calle(), "tortuga", "tortuga.jpg", 2, true);
		prueba = 0;
		generarCasillaTrampa();
		generarCasillaSuperpoder();
		generarCasillaArbol();
		setCorredorActivo();
	}
	
	public void imprimirCasillas() {
		System.out.println("Casilla trampa: " + casillaTrampa);
		System.out.println("Casilla superpoder: " + casillaSuperpoder);
		System.out.println("Casilla arbol: " + casillaArbol);
		if (trampaParaLiebre)
			System.out.println("Casilla trampa: liebre");
		else
			System.out.println("Casilla trampa: tortuga");
	}

	public void setCorredorActivo() {
		if (liebre.getTurno())
			corredorActivo = liebre;
		else
			corredorActivo = tortuga;
	}

	public boolean lanzarDado() {
		boolean isPosible = false;
		Dado.lanzar(corredorActivo.getDesplazamiento());
		if (corredorActivo.getPosicion() + Dado.getValor() < Calle.DIM)
			isPosible = true;
		else
			cambiarTurnos();
		return isPosible;
	}

	public void cambiarTurnos() {
		liebre.cambiaTurno();
		int i = 8;
		tortuga.cambiaTurno();
		setCorredorActivo();
	}

	private boolean isJugadaCorrecta(int i) {
		return (corredorActivo.getPosicion() + Dado.getValor() == i+prueba);
	}

	public boolean resolverJugada(int i) {
		boolean resuelta = false;

		// Comprobamos que se trata de ir a la casilla correcta
		if (isJugadaCorrecta(i)) {
			corredorActivo.setPosicion(corredorActivo.getPosicion() + Dado.getValor());
			// Incrementamos la puntuacion del corredor
			corredorActivo.incrementaPuntuacion(
					corredorActivo.getCalleAsignada().puntosCasilla(corredorActivo.getPosicion()) * Dado.getValor());

			// Llegamos a la meta
			if (isPartidaFinalizada())
				corredorActivo.incrementaPuntuacion(200);

			// Dormir liebre.
			if (caisteEnArbol(i) && !liebre.estaDurmiendo()) {
				cambiarTurnos();
				liebre.dormir();
			}

			// Despertar liebre.
			if (caisteEnArbol(i) && liebre.estaDurmiendo()) {
				liebre.despertar();
			}

			cambiarTurnos();
			resuelta = true;
		}
		return resuelta;
	}

	public boolean caisteEnLaTrampa(int i) {
		if (getCorredorNoActivo().equals(liebre) && trampaParaLiebre
				&& getCorredorNoActivo().getPosicion() == casillaTrampa)
			return true;
		if (getCorredorNoActivo().equals(tortuga) && trampaParaTortuga
				&& getCorredorNoActivo().getPosicion() == casillaTrampa)
			return true;
		return false;
	}

	public boolean caisteEnElSuperpoder(int i) {
		if (getCorredorNoActivo().getPosicion() == casillaSuperpoder)
			return true;
		return false;
	}

	public boolean teSalvaste(int i) {
		if (corredorActivo.tieneSuperPoder())
			return true;
		return false;
	}

	public boolean isPartidaFinalizada() {
		return (liebre.getPosicion() == Calle.POSICION_META || tortuga.getPosicion() == Calle.POSICION_META);
	}

	public Corredor getLiebre() {
		return liebre;
	}

	private void generarCasillaTrampa() {
		int aleatorio = (int) (Math.random() * 10) + 1;
		casillaTrampa = (int) (Math.random() * 9) + 1;

		if (aleatorio % 10 == 0)
			asignarTrampaLiebre();
		else
			asignarTrampaTortuga();
	}

	private void asignarTrampaLiebre() {
		trampaParaTortuga = false;
		trampaParaLiebre = true;
	}

	private void asignarTrampaTortuga() {
		trampaParaTortuga = true;
		trampaParaLiebre = false;
	}

	private void generarCasillaSuperpoder() {
		casillaSuperpoder = (int) (Math.random() * 9) + 1;
		while (casillaSuperpoder == casillaTrampa)
			generarCasillaSuperpoder();
	}

	private void generarCasillaArbol() {
		casillaArbol = (int) (Math.random() * 9) + 1;
		while (casillaArbol == casillaTrampa || casillaArbol == casillaSuperpoder)
			generarCasillaArbol();
	}

	public int getCasillaSuperPoder() {
		return casillaSuperpoder;
	}

	public int getCasillaTrampa() {
		return casillaTrampa;
	}

	public Corredor getCorredorActivo() {
		return corredorActivo;
	}

	public Corredor getCorredorNoActivo() {
		if (corredorActivo.equals(liebre))
			return tortuga;
		return liebre;
	}

	public Corredor getTortuga() {
		return tortuga;
	}

	public int getCasillaArbol() {
		return casillaArbol;
	}

	public boolean caisteEnArbol(int posicion) {
		if (getLiebre().equals(getCorredorNoActivo()) && getLiebre().getPosicion() == casillaArbol)
			return true;
		return false;
	}

}