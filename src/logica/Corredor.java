package logica;

public class Corredor {
	private int puntuacion;
	private int posicion;
	private int desplazamiento;
	private boolean turno;
	private String nombre;
	private String foto;
	private Calle calleAsignada;
	private boolean superPoder;
	private boolean estaDurmiendo;
	public final static int POSICION_SALIDA = 1;

	public Corredor(Calle calleAsignada, String nombre, String foto, int desplazamiento, boolean turno) {
		this.calleAsignada = calleAsignada;
		this.nombre = nombre;
		this.foto = foto;
		this.desplazamiento = desplazamiento;
		this.turno = turno;
		this.puntuacion = 0;
		this.posicion = POSICION_SALIDA;
	}

	public String getNombre() {
		return nombre;
	}

	public int getDesplazamiento() {
		return desplazamiento;
	}

	public int getPuntuacion() {
		return puntuacion;
	}

	public int getPosicion() {
		return posicion;
	}

	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}

	public Calle getCalleAsignada() {
		return calleAsignada;
	}

	public String getFoto() {
		return foto;
	}

	public void incrementaPuntuacion(int puntos) {
		puntuacion += puntos;
	}

	public void recogerSuperpoder() {
		puntuacion += 500;
		superPoder = true;
	}

	public void caerEnLaTrampa() {
		puntuacion = 0;
	}

	public void sobrevivirTrampa() {
		puntuacion += 1000;
	}

	public boolean tieneSuperPoder() {
		return superPoder;
	}

	public void cambiaTurno() {
		turno = !turno;
	}

	public boolean getTurno() {
		return turno;
	}

	public boolean estaDurmiendo() {
		return estaDurmiendo;
	}

	public void dormir() {
		estaDurmiendo = true;
	}

	public void despertar() {
		estaDurmiendo = false;
	}
	
	public void comer() {
		System.out.print("�am �am");
	}
	
	public void engulle() {
		System.out.print("*engullendo*");
	}
	
	
	@Override
	public String toString() {
		return "Corredor [puntuacion=" + puntuacion + ", posicion=" + posicion + ", desplazamiento=" + desplazamiento
				+ ", turno=" + turno + ", nombre=" + nombre + ", foto=" + foto + ", calleAsignada=" + calleAsignada
				+ ", superPoder=" + superPoder + ", estaDurmiendo=" + estaDurmiendo + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((calleAsignada == null) ? 0 : calleAsignada.hashCode());
		result = prime * result + desplazamiento;
		result = prime * result + (estaDurmiendo ? 1231 : 1237);
		result = prime * result + ((foto == null) ? 0 : foto.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + posicion;
		result = prime * result + puntuacion;
		result = prime * result + (superPoder ? 1231 : 1237);
		result = prime * result + (turno ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Corredor other = (Corredor) obj;
		if (calleAsignada == null) {
			if (other.calleAsignada != null)
				return false;
		} else if (!calleAsignada.equals(other.calleAsignada))
			return false;
		if (desplazamiento != other.desplazamiento)
			return false;
		if (estaDurmiendo != other.estaDurmiendo)
			return false;
		if (foto == null) {
			if (other.foto != null)
				return false;
		} else if (!foto.equals(other.foto))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (posicion != other.posicion)
			return false;
		if (puntuacion != other.puntuacion)
			return false;
		if (superPoder != other.superPoder)
			return false;
		if (turno != other.turno)
			return false;
		return true;
	}
}
